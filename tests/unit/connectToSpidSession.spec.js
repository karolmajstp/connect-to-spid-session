/*globals describe,it,afterEach,beforeEach,document*/
'use strict';

var expect = require('chai').expect;
var hoistNonReactStatics = require('hoist-non-react-statics');
var mockery = require('mockery');
var inherits = require('inherits');
var React;
var ReactTestUtils;
var connectToSpidSession;
var provideContext;
var SpidSessionStore = require('../../stores/SpidSessionStore');
var MockedSpidSessionStore;
var createMockComponentContext = require('fluxible/utils/createMockComponentContext');
var jsdom = require('jsdom');

describe('connectToSpidSession', () => {
    var appContext;

    beforeEach((done) => {
        MockedSpidSessionStore = function (dispatcher) { SpidSessionStore.call(this, dispatcher) };

        inherits(MockedSpidSessionStore, SpidSessionStore);
        hoistNonReactStatics(MockedSpidSessionStore, SpidSessionStore);

        MockedSpidSessionStore.prototype.setSession = function (status, data) {
            this.status = status;
            this.data = data;
            this.emitChange();
        };

        jsdom.env('<html><body></body></html>', [], (err, window) => {
            if (err) {
                done(err);
            }
            global.window = window;
            global.document = window.document;
            global.navigator = window.navigator;

            appContext = createMockComponentContext({
                stores: [MockedSpidSessionStore]
            });

            mockery.enable({
                warnOnReplace: false,
                warnOnUnregistered: false,
                useCleanCache: true
            });
            React = require('react/addons');
            ReactTestUtils = require('react/lib/ReactTestUtils');
            connectToSpidSession = require('../../').connectToSpidSession;
            provideContext = require('fluxible-addons-react').provideContext;

            done();
        });
    });

    afterEach(() => {
        //delete global.window;
        delete global.document;
        delete global.navigator;
        mockery.disable();
    });

    it('should get the state from the SpidSessionStore', () => {
        var Component = React.createClass({
            contextTypes: {
                executeAction: React.PropTypes.func.isRequired
            },

            render: function() {
                return (
                    <div>
                        <span id="foo">{this.props.session.status}</span>
                    </div>
                );
            }
        });
        var WrappedComponent = provideContext(connectToSpidSession(Component, { prop: 'session' }));

        var container = document.createElement('div');
        var component = React.render(<WrappedComponent context={appContext} />, container);
        var domNode = component.getDOMNode();
        expect(domNode.querySelector('#foo').textContent).to.equal('unknown');

        appContext.getStore(MockedSpidSessionStore).setSession('logged_in', {});

        expect(domNode.querySelector('#foo').textContent).to.equal('logged_in');
    });

    it('should get the state from the SpidSessionStore used as annotation', function () {
        @connectToSpidSession({
            prop: 'session'
        })
        class Component extends React.Component {
            static contextTypes = {
                executeAction: React.PropTypes.func.isRequired
            }

            render() {
                return (
                    <div>
                        <span id="foo">{this.props.session.status}</span>
                    </div>
                );
            }
        }

        var WrappedComponent = provideContext(Component);

        var container = document.createElement('div');
        var component = React.render(<WrappedComponent context={appContext} />, container);
        var domNode = component.getDOMNode();
        expect(domNode.querySelector('#foo').textContent).to.equal('unknown');

        appContext.getStore(MockedSpidSessionStore).setSession('logged_in', {});

        expect(domNode.querySelector('#foo').textContent).to.equal('logged_in');
    });
});
