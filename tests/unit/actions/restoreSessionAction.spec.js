var chai = require('chai');
var sinon = require('sinon');
var sinonChai = require('sinon-chai');
chai.use(sinonChai);
var expect = chai.expect;

var spidActions = require('../../../actions/spidActions');
var restoreSessionAction = spidActions.restoreSessionAction;
var createMockActionContext = require('fluxible/utils/createMockActionContext');

describe('restoreSessionAction', function () {
    var actionContext, dispatchStub;

    beforeEach(function () {
        actionContext = createMockActionContext();

        dispatchStub = sinon.stub(actionContext, 'dispatch');
    });

    afterEach(function () {
        dispatchStub.restore();
    });

    it('dispatch SPID_SESSION_RESTORE event with payload', function (done) {
        var payload = { userId: 1 };

        actionContext.executeAction(restoreSessionAction, payload, function (err) {
            if (err) {
                return done(err);
            }

            expect(dispatchStub).to.have.been.calledWith('SPID_SESSION_RESTORE', payload);
            done();
        });
    })
});
