# Quick start

Add *SpidSessionStore* to your application

```
import Fluxible from 'fluxible';
import { SpidSessionStore } from 'connect-to-spid-session';

const app = new Fluxible({ component: Application });
app.registerStore(SpidSessionStore);
```

Execute initialize in your Application component

```
import { spidActions } from 'connect-to-spid-session';

const SPID_CONFIG =  {
    client_id: 'SPID_CLIENT_ID',
    server: 'SPID_SERVER',
    prod: process.env.NODE_ENV === 'production',
    logging: true,
    session: process.env.BROWSER ? null : 'placeholder',
    cookie: process.env.BROWSER ? true : false
};
...
componentDidMount() {
    this.context.executeAction(spidActions.initializeSpidAction, SPID_CONFIG);
}
```

Wrap your component with higher order component:

```
import { connectToSpidSession } from 'connect-to-spid-session';

@connectToSpidSession({ prop: 'spidSession' })

class MyComponent extends Componen {
```

and use `this.props.spidSession` to access session information.

Session object passed as property has field status and data, for example:

```
{
    status: 'logged_out',
    data: null
}
```
