# API: `restoreSessionAction`

You can use `restoreSessionAction` to initialize session manually. It is usefull when you want to keep in sync session
on frontend and backend.

As a payload pass raw session data object. Passing falsy values is noop.

Example of usage:

```
import { spidActions } from 'connect-to-spid-session';

function renderer(req, res, next) {

  // Create fluxible context
  const context = app.createContext({
    req
  });

  // Get stored session object
  let session = req.session.data;

  context.executeAction(spidActions.restoreSessionAction, session)
    .then(() => {
        //Do actual rendering
    });
```
