# API Documentation

* [connectToSpidSession](./connectToSpidSession.md)
* [initializeSpidAction](./initializeSpidAction.md)
* [logoutAction](./logoutAction.md)
* [restoreSessionAction](./restoreSessionAction.md)
* [SpidSessionStore](./SpidSessionStore.md)
