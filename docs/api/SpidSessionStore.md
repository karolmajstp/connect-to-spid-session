# API: `SpidSessionStore`

`SpidSessionStore` keeps session status and data. It listen to `SPID_INIT` event to initialize SPiD sdk.
Listen to `SPID_SESSION_RESTORE` event to initialize user session.

This store must be added to fluxible application, like this:

```
import Fluxible from 'fluxible';
import { SpidSessionStore } from 'connect-to-spid-session';

const app = new Fluxible({ component: Application });
app.registerStore(SpidSessionStore);
```
