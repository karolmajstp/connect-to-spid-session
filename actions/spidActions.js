var VGS = require('../utils/spid-sdk');

function initializeSpidAction(context, payload, done) {
    context.dispatch('SPID_INIT', payload);
    done();
}

function restoreSessionAction(context, payload, done) {
    context.dispatch('SPID_SESSION_RESTORE', payload);
    done();
}

function logoutAction(context, payload, done) {
    VGS.Auth.logout();
    done();
}

module.exports = {
    initializeSpidAction: initializeSpidAction,
    restoreSessionAction: restoreSessionAction,
    logoutAction: logoutAction
};
